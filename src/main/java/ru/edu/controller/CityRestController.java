package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.edu.service.CityInfo;
import ru.edu.service.CityService;
import java.util.List;

@RestController
@RequestMapping(value = "api/travel", produces = MediaType.ALL_VALUE)
public class CityRestController {

    private CityService cityService;

    @Autowired
    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }

    /**
     * Get all.
     *
     */
    // GET api/travel/all
    @GetMapping("/all")
    public List<CityInfo> getAll() {
        return cityService.getAll();
    }

    /**
     * Get city by id. Returns null if not found.
     *
     * @param cityId - item id
     */
    // GET api/travel?cityId=
    @GetMapping
    public CityInfo getCity(@RequestParam("cityId") String cityId) {
        return cityService.getCity(cityId);
    }

    /**
     * Create new city.
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public CityInfo create(@RequestBody CityInfo info) {
        return cityService.create(info);
    }

    /**
     * Update existing city. Don't change id
     */
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public CityInfo update(@RequestBody CityInfo info) {
        return cityService.update(info);
    }

    /**
     * Delete city by id.
     *
     */
    // DELETE api/travel?cityIdToDelete=
    @DeleteMapping
    public CityInfo delete(@RequestParam("cityIdToDelete") String cityId) {
        return cityService.delete(cityId);
    }

    // GET api/travel?cityIdWeather=
    @GetMapping(consumes = MediaType.ALL_VALUE)
    public String getWeather(@RequestParam("cityIdWeather") String cityId) {
        return cityService.getWeather(cityId);
    }
}
